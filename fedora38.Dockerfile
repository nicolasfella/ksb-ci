FROM fedora:38

RUN sudo dnf in -y git perl perl-IPC-Cmd perl-MD5 perl-FindBin

RUN groupadd -r user && useradd --create-home --gid user user && echo 'user ALL=NOPASSWD: ALL' > /etc/sudoers.d/user

USER user
