FROM ubuntu:23.04

RUN apt update && apt full-upgrade -y && apt install -y sudo git

RUN groupadd -r user && useradd --create-home --gid user user && echo 'user ALL=NOPASSWD: ALL' > /etc/sudoers.d/user

USER user
